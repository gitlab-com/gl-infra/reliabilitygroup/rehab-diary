# Rehab Diary

***

## Description
An issue tracker to track interrupt work and issues that Rehab works on outside of the current assignments and areas of focus.

## Visuals
![img](https://gitlab.com/gitlab-com/gl-infra/reliability/rehab-diary/-/raw/main/1637574019736.jpg)

## Usage
Most meaningful information in this project are going to be under `Issues`.

## Project status
Ongoing
